Window.prototype.ACLICKER_INTERVAL = undefined
Window.prototype.setClicker = setClicker
Window.prototype.stopClicker = stopClicker

const button = document.querySelector('button.block.lg\\:hidden')

const setClicker = () => {
  if (window.ACLICKER_INTERVAL !== undefined) return

  window.ACLICKER_INTERVAL = setInterval(() => {
    button.click()
  }, 750)
}

const stopClicker = () => {
  if (window.ACLICKER_INTERVAL === undefined) return

  clearInterval(window.ACLICKER_INTERVAL)
}

setClicker()
